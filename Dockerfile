FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive

# Install NgINX from nginx home
RUN apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 && \
    echo 'deb http://nginx.org/packages/ubuntu/ xenial nginx' > /etc/apt/sources.list.d/nginx.list
    
RUN apt-get update -y 
RUN apt-get install -y \
    nginx \
    wget \
    unzip \
    supervisor && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*

# Configure Nginx
ADD build/nginx.conf /etc/nginx/nginx.conf
COPY build/html/ /var/www/html/

VOLUME [ "/var/www" ]

EXPOSE 80
EXPOSE 443

WORKDIR /tmp

ADD build/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord"]
